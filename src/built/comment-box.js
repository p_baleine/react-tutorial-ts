var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
(function (factory) {
    if (typeof module === 'object' && typeof module.exports === 'object') {
        var v = factory(require, exports); if (v !== undefined) module.exports = v;
    }
    else if (typeof define === 'function' && define.amd) {
        define(["require", "exports", "react", "jquery"], factory);
    }
})(function (require, exports) {
    "use strict";
    var React = require("react");
    var $ = require("jquery");
    var Comment = (function (_super) {
        __extends(Comment, _super);
        function Comment() {
            _super.apply(this, arguments);
        }
        Comment.prototype.render = function () {
            return (React.createElement("div", {className: "comment"}, React.createElement("h2", {className: "commentAuthor"}, this.props.author), this.props.text));
        };
        return Comment;
    }(React.Component));
    var CommentList = (function (_super) {
        __extends(CommentList, _super);
        function CommentList() {
            _super.apply(this, arguments);
        }
        CommentList.prototype.render = function () {
            var commentNodes = this.props.data.map(function (comment) {
                return (React.createElement(Comment, {author: comment.author, key: comment.key, text: comment.text}));
            });
            return (React.createElement("div", {className: "commentList"}, commentNodes));
        };
        return CommentList;
    }(React.Component));
    var CommentForm = (function (_super) {
        __extends(CommentForm, _super);
        function CommentForm() {
            _super.call(this);
            this.state = { author: "", text: "" };
            this.handleAuthorChange = this.handleAuthorChange.bind(this);
            this.handleTextChange = this.handleTextChange.bind(this);
            this.handleSubmit = this.handleSubmit.bind(this);
        }
        CommentForm.prototype.handleAuthorChange = function (e) {
            this.setState({ author: e.target.value });
        };
        CommentForm.prototype.handleTextChange = function (e) {
            this.setState({ text: e.target.value });
        };
        CommentForm.prototype.handleSubmit = function (e) {
            e.preventDefault();
            var author = this.state.author.trim();
            var text = this.state.text.trim();
            if (!text || !author) {
                return;
            }
            this.props.onCommentSubmit({ author: author, text: text });
            this.setState({ author: "", text: "" });
        };
        CommentForm.prototype.render = function () {
            return (React.createElement("form", {className: "commentForm", onSubmit: this.handleSubmit}, React.createElement("input", {type: "text", placeholder: "Your name", value: this.state.author, onChange: this.handleAuthorChange}), React.createElement("input", {type: "text", placeholder: "Say something...", value: this.state.text, onChange: this.handleTextChange}), React.createElement("input", {type: "submit", value: "Post"})));
        };
        return CommentForm;
    }(React.Component));
    var CommentBox = (function (_super) {
        __extends(CommentBox, _super);
        function CommentBox() {
            _super.call(this);
            this.state = { data: [] };
            this.loadCommentsFromServer = this.loadCommentsFromServer.bind(this);
            this.handleCommentSubmit = this.handleCommentSubmit.bind(this);
        }
        CommentBox.prototype.componentDidMount = function () {
            this.loadCommentsFromServer();
            if (this.props.pollInterval) {
                setInterval(this.loadCommentsFromServer, this.props.pollInterval);
            }
        };
        CommentBox.prototype.render = function () {
            return (React.createElement("div", {className: "commentBox"}, React.createElement(CommentList, {data: this.state.data}), React.createElement(CommentForm, {onCommentSubmit: this.handleCommentSubmit})));
        };
        CommentBox.prototype.loadCommentsFromServer = function () {
            $.ajax({
                url: this.props.url,
                dataType: "json",
                cache: false,
                success: function (data) {
                    this.setState({ data: this.convertKey(data) });
                }.bind(this),
                error: function (xhr, status, err) {
                    console.error(this.props.url, status, err.toString());
                }.bind(this)
            });
        };
        CommentBox.prototype.handleCommentSubmit = function (comment) {
            $.ajax({
                url: this.props.url,
                dataType: "json",
                type: "POST",
                data: comment,
                success: function (data) {
                    this.setState({ data: data });
                }.bind(this),
                error: function (xhr, status, err) {
                    console.error(this.props.url, status, err.toString());
                }.bind(this)
            });
        };
        CommentBox.prototype.convertKey = function (data) {
            return data.map(function (datum) {
                datum.key = datum.id;
                return datum;
            });
        };
        return CommentBox;
    }(React.Component));
    exports.CommentBox = CommentBox;
});
