/// <reference path="../typings/bundle.d.ts" />

import * as React from "react";
import * as $ from "jquery";

/*
* Comment
*/

export interface CommentState {}

export interface CommentProps {
  author: string,
  text: string,
  key?: number
}

class Comment extends React.Component<CommentProps, CommentState> {
  render() {
    return (
      <div className="comment">
        <h2 className="commentAuthor">
          {this.props.author}
        </h2>
        {this.props.text}
      </div>
    );
  }
}

/*
* CommentList
*/

export interface CommentListState {}

export interface CommentListProps {
  data: CommentProps[]
}

class CommentList extends React.Component<CommentListProps, CommentListState> {
  render() {
    var commentNodes = this.props.data.map(comment => {
      return (
        <Comment author={comment.author} key={comment.key}
                 text={comment.text} />
      );
    });

    return (
      <div className="commentList">
        {commentNodes}
      </div>
    );
  }
}

/*
* CommentForm
*/

export interface CommentFormState {}

export interface CommentFormProps {
  onCommentSubmit: (CommentProps) => any;
}

class CommentForm extends React.Component<CommentFormProps, CommentFormState> {
  state: CommentProps = {author: "", text: ""}

  constructor() {
    super();
    this.handleAuthorChange = this.handleAuthorChange.bind(this);
    this.handleTextChange = this.handleTextChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleAuthorChange(e) {
    this.setState({author: e.target.value});
  }

  handleTextChange(e) {
    this.setState({text: e.target.value});
  }

  handleSubmit(e) {
    e.preventDefault();
    var author = this.state.author.trim();
    var text = this.state.text.trim();
    if (!text || !author) {
      return;
    }
    this.props.onCommentSubmit({author: author, text: text});
    this.setState({author: "", text: ""});
  }

  render() {
    return (
      <form className="commentForm" onSubmit={this.handleSubmit}>
        <input type="text"
               placeholder="Your name"
               value={this.state.author}
               onChange={this.handleAuthorChange} />
        <input type="text"
               placeholder="Say something..."
               value={this.state.text}
               onChange={this.handleTextChange} />
        <input type="submit" value="Post" />
      </form>
    );
  }
}

/*
* CommentBox
*/

export interface CommentBoxState {
  data: CommentProps[]
}

export interface CommentBoxProps {
  url: string,
  pollInterval?: number
}

export interface CommentDatum {
  author: string,
  text: string,
  id: number,
  key?: number
}

export class CommentBox extends
React.Component<CommentBoxProps, CommentBoxState> {
  state: CommentBoxState = {data: []}

  constructor() {
    super();
    this.loadCommentsFromServer = this.loadCommentsFromServer.bind(this);
    this.handleCommentSubmit = this.handleCommentSubmit.bind(this);
  }

  componentDidMount() {
    this.loadCommentsFromServer();
    if (this.props.pollInterval) {
      setInterval(this.loadCommentsFromServer, this.props.pollInterval);
    }
  }

  render() {
    return (
      <div className="commentBox">
        <CommentList data={this.state.data} />
        <CommentForm onCommentSubmit={this.handleCommentSubmit}/>
      </div>
    );
  }

  loadCommentsFromServer() {
    $.ajax({
      url: this.props.url,
      dataType: "json",
      cache: false,
      success: function(data) {
        this.setState({ data: this.convertKey(data) });
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  }

  handleCommentSubmit(comment: CommentProps) {
    $.ajax({
      url: this.props.url,
      dataType: "json",
      type: "POST",
      data: comment,
      success: function(data) {
        this.setState({ data: data });
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  }

  convertKey(data: CommentDatum[]): CommentProps[] {
    return data.map(datum => {
      datum.key = datum.id;
      return datum;
    });
  }
}
