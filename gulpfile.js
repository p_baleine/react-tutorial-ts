"use strict";

var gulp = require("gulp");
var typescript = require("typescript");
var ts = require("gulp-typescript");
var browserify = require("browserify");
var source = require("vinyl-source-stream");
var del = require("del");
var watch = require('gulp-watch');
var batch = require('gulp-batch');

var project = ts.createProject("src/tsconfig.json", { typescript: typescript });

gulp.task("compile", function() {
    var result = gulp.src("src/**/*{ts,tsx}")
            .pipe(ts(project));
    return result.js.pipe(gulp.dest(".tmp"));
});

gulp.task("bundle", ["compile"], function() {
    var b = browserify(".tmp/bootstrap.js");
    return b.bundle()
        .pipe(source("bundle.js"))
        .pipe(gulp.dest("public"));
});

gulp.task("clean", function(done) {
    del([".tmp"], done.bind(this));
});

gulp.task("watch", function() {
    gulp.watch("src/**/*{ts,tsx}", ["bundle"]);
});
